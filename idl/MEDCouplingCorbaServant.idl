// Copyright (C) 2007-2012  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef __MEDCOUPLINGCORBASERVANT_IDL__
#define __MEDCOUPLINGCORBASERVANT_IDL__

#include "SALOME_Types.idl"
#include "SALOME_GenericObj.idl"

module SALOME_MED
{
  interface MEDCouplingTimeLabelCorbaInterface
  {
    long getTimeLabel();
  };

  interface MEDCouplingRefCountCorbaInterface : SALOME::ExportableObject, MEDCouplingTimeLabelCorbaInterface
  {
  };

  interface DataArrayDoubleCorbaInterface : MEDCouplingRefCountCorbaInterface
  {
    void getTinyInfo(out SALOME_TYPES::ListOfLong la, out SALOME_TYPES::ListOfString sa);
    void getSerialisationData(out SALOME_TYPES::ListOfDouble da);
  };

  interface DataArrayIntCorbaInterface : MEDCouplingRefCountCorbaInterface
  {
    void getTinyInfo(out SALOME_TYPES::ListOfLong la, out SALOME_TYPES::ListOfString sa);
    void getSerialisationData(out SALOME_TYPES::ListOfLong la);
  };

  interface MEDCouplingMeshCorbaInterface : MEDCouplingRefCountCorbaInterface
  {
    //!CORBA inplementation of MEDCouplingPointSet::getTinySerializationInformation
    void getTinyInfo(out SALOME_TYPES::ListOfDouble da, out SALOME_TYPES::ListOfLong la, out SALOME_TYPES::ListOfString sa);
    void getSerialisationData(out SALOME_TYPES::ListOfLong la, out SALOME_TYPES::ListOfDouble da);
  };

  interface MEDCouplingPointSetCorbaInterface : MEDCouplingMeshCorbaInterface
  {
    DataArrayDoubleCorbaInterface getCoords();
  };

  interface MEDCouplingUMeshCorbaInterface : MEDCouplingPointSetCorbaInterface
  {
  };

  interface MEDCouplingExtrudedMeshCorbaInterface : MEDCouplingMeshCorbaInterface
  {
  };

  interface MEDCouplingCMeshCorbaInterface : MEDCouplingMeshCorbaInterface
  {
  };

  interface MEDCouplingFieldCorbaInterface : MEDCouplingRefCountCorbaInterface
  {
    MEDCouplingMeshCorbaInterface getMesh();
  };
  
  interface MEDCouplingFieldTemplateCorbaInterface : MEDCouplingFieldCorbaInterface
  {
    //!returns the 3 tiny arrays to prepare the new instance locally.
    void getTinyInfo(out SALOME_TYPES::ListOfLong la, out SALOME_TYPES::ListOfDouble da, out SALOME_TYPES::ListOfString sa);
    void getSerialisationData(out SALOME_TYPES::ListOfLong la);
  };

  interface MEDCouplingFieldDoubleCorbaInterface : MEDCouplingFieldCorbaInterface
  {
    //!returns the 3 tiny arrays to prepare the new instance locally.
    void getTinyInfo(out SALOME_TYPES::ListOfLong la, out SALOME_TYPES::ListOfDouble da, out SALOME_TYPES::ListOfString sa);
    void getSerialisationData(out SALOME_TYPES::ListOfLong la, out SALOME_TYPES::ListOfDouble2 da2);
  };

  typedef sequence<MEDCouplingMeshCorbaInterface> MEDCouplingMeshesCorbaInterface;

  interface MEDCouplingMultiFieldsCorbaInterface : MEDCouplingRefCountCorbaInterface
  {
    long getMainTinyInfo(out SALOME_TYPES::ListOfLong la, out SALOME_TYPES::ListOfDouble da, out long nbOfArrays, out long nbOfFields);
    //!for field templates
    void getTinyInfo(in long id, out SALOME_TYPES::ListOfLong la, out SALOME_TYPES::ListOfDouble da, out SALOME_TYPES::ListOfString sa);
    void getSerialisationData(in long id, out SALOME_TYPES::ListOfLong la);
    //!for arrays
    DataArrayDoubleCorbaInterface getArray(in long id);
    //! for meshes
    MEDCouplingMeshesCorbaInterface getMeshes();
    MEDCouplingMeshCorbaInterface getMeshWithId(in long id);
  };

  interface MEDCouplingFieldOverTimeCorbaInterface : MEDCouplingMultiFieldsCorbaInterface
  {
    void getTinyInfoAboutTimeDefinition(out SALOME_TYPES::ListOfLong la, out SALOME_TYPES::ListOfDouble da);
  };
};

#endif
